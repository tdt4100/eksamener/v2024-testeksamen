## Eksamensstruktur
-	Fem deler
-	Hver del teller 20%
-	Del 1 består av 10 oppgaver dere dere skal kommentere ut den eller de linjene som er sanne. Her kan du gjerne skrive mainfunksjoner som tester påstandene. Dette blir som utseende flervalgsoppgaver, men det er faktisk programmering.
-	Del 2-5 er én felles oppgave, men vi har forsøkt å separere delene rimelig godt. Det kan være at du også i del 1 finner referanser til klasser som brukes i del 2-5.
-	Det betyr ikke at det er vanntette skott mellom delene, dog!
-	Eksamen har atskillig færre kodelinjer enn tidligere eksamener. Vi ønsker ikke at dere skal bruke tid på å skrive de samme ‘typene’ av kode flere ganger, og bruke tid på ting uansett som ikke har gitt mye poeng.
-	Det vil vies mer tid til å få frem kunnskap rundt de ulike elementene i faget, og samhandlingen mellom dem. I hver del kan det eksistere hjelpeklasser og Interface som dere må forholde dere til, gjerne kun i den delen. Mer tenking, mindre panisk koding.
-	Dere vil finne kode dere må forholde dere til på ulike steder (struktur som et ordentlig javaprosjekt, dette er eksempel på navn)
o	Oppgavene, som src/main/java/com/diablo/characterregistry/part2
o	Hjelpekode, som src/main/java/no/ntnu/tdt4100/part2. Her vil f.eks. Interface og hjelpeklasser ligge. Her skal dere ikke skrive noen egen kode.
o	Fil-repos, som src/main/resources/ (denne er ikke med i testeksamen)
o	Tester, som src/test/java/com/diablo/characterregistry/part2
-	Dette er en eksamen som er bygget rundt noe dere kjenner godt til fra øvingene – tester. Det betyr ikke at man ikke kan kode uten å se på testene, men de er en god start til å få opp strukturen. Mer om tester under.
-	Mens vi tidligere eksamener har åpnet opp for bruk av main, så har det blitt komplisert å bruke det på mer avanserte måter enn de enkleste metodene. Det koster tid å sette seg inn i hva det gjør. Testene vil være mer praktiske i så måte. Og mer slik man gjør det i industrien.
- Beskrivelsen av krav til klasser og metoder står i javadocen for klassene/metodene. Vi har tidligere år også hatt en beskrivelse av hovedpunktet til en metode i en mdfil. Erfaringene med dette har vært delte. Mange studenter ender opp med å bare kode de kravene som står i denne filen, og leser ikke de resterende kravene i javadocen. At vi skal skrive ting to steder kan også introdusere større mulighet for feil fra vår side. Hvorfor ønsker vi å legge ting i JD i stedet for i en fil? Fordi en da kan holde musen over en metode, eller et metodekall, og da får en opp hele JD som en fint strukturert ting. 
-	IKKE endre på filer som man ikke ber dere om å endre på. Man skal heller ikke, med mindre det sies, ikke legge til publicmetoder i en klasse og bruke det et annet sted. Ekstra privatemetoder går derimot helt fint! Hvorfor ikke tillate flere publicmetoder? Fordi du av prinsipp ikke skal modifisere en klasses utseende (tilgjengelige metoder) sånn uten videre.
-	Noen spurte på forelesning om ‘source action’ var tilgjengelig på eksamen (generate getters etc). Ja, det er tilgjengelig.

## Testing
-	Forbered deg på å lese tester! 
-	Du har IKKE tilgang på ‘testfanen’ til venstre i VS Code (Erlenmeyerkolben), så du må åpne testene som filer og kjøre dem derfra.
-	Del 1, 2 og 3 har tester som dekker 20% av poengene (sånn omtrentlige prosent prosent altså, inntil videre.)
o	Vi har forsøkt å dekke de enkleste 20% av denne delen med testene, men det betyr ikke at akkurat du synes det samme.
-	Del 4 og 5 har tester som dekker 100% av poengene 
-	Bruk testene som er gitt!
-	Testene inneholder ting som ligger på utsiden av pensum, men de er der for å sette testen i en korrekt tilstand som gjør det mulig å kjøre ting. Et eksempel er når dere møter ting som mock().  
o	Denne betyr at pcr ikke egentlig lages ved å kalle en konstruktør i PlayerCharacterRegister, men i stedet en fuskeklasse. Linjen med ‘when’ sier at når etterfølgende kode kalles med metodenavn totalParagons og parameter “Demon Hunter” så skal dette kallet returnere 9942. På den måten kan man sette opp systemet i en tilfusket tilstand som gjør det mulig å teste enkelte ting på en enkel måte – man trenger ikke engang å implementere metoden totalParagon!
o	Typiske ting dere møter som har med mock å gjøre er: when(), thenReturn()/thenThrow(), verify() og doNothing().

Ta koden
  ```java
  PlayerCharacterRegister pcr = mock();
  when(pcr.totalParagons("Demon Hunter")).thenReturn(9942);
  ```
...som betyr at når pcr sin metode totalParagons kalles med argument "Demon Hunter" så skal pcr late som det er et godt kodet PCR og returnere 9942. Et annet eksempel er 
 ```java
doNothing().when(objekt1).doSomething(any(Klasse.class), anyInt())
  ```
som betyr 'Når metoden *doSomething* kalles på objektet *objekt1*, med et hvilket som helst objekt av type *Klasse* og et hvilket som helst *heltall* som argument, så skal mockito ikke gjøre noe som helst.

 ```java
verify(mockObject, times(1)).doSomething(any(Klasse.class), anyInt());
 ```
 betyr at vi verifiserer at metoden *doSomething* har blitt kalt i *mock-objektet*, (med to spesifikke parametre som i oppgaven over) nøyaktig *én gang*.


o	På samme måte brukes en [Fake-klasse](/src/main/java/com/diablo/manufacturing/examples/part2/FakeOutputStream.java) for å jukse tilsvarende i en oppgave som gjelder filer. 
-	Du kan kjøre én og én test.
-	Du kan debugge en test med høyreklikk. Stoppunkt kan legges inn både i testfilen og i andre klasser.
-	Du kan legge inn printlns (også i andre klasser enn testklassene), vises (i debugtabben mener jeg)
-	Du kan utvide testene om du vil, eller lage egne (lurt å ikke fjerne det som står da, kommenter heller ut ting du ikke ønsker å teste akkurat nå)
-	Noen av testene er kommentert ut, det er fordi de skal feile før du gjør noe med koden. Innimellom inneholder testkoden referanser til klasser som dere ikke har definert ennå, og da vil jo koden ikke bygge og testen referere til noe feil. I stedet så feiler testen på assertFalse(true). Dette står klart beskrevet på eksamen, og testene i [Part2Test.java](/src/test/java/com/diablo/manufacturing/examples/part2/Part2Test.java) viser dette.
-	Du kan velge å ikke forholde deg til hva mock holder på med hvis du ikke ønsker det, men kun lese testnavn og kanskje ta en kikk på de metodekallene som gjøres. Det burde uansett kunne hjelpe deg til å se hvorfor testen eventuelt feiler. Husk at du også kan bruke testen til å oppnå en tilstand, og så kan du sette inn et stoppunkt for å undersøke hva slags tilstand de ulike objektene er i!

## Hva bør du gjøre
-	Ta en rask gjennomgang av hele eksamen først – skaff deg et overblikk av hva hver seksjon handler om. 
-	Før du går i gang med hver del så ser du litt nøyere på hele delen. Hvilke klasser skal du fikse på, hvilke finnes fra før.
-	Før du koder direkte på en klasse, les raskt igjennom den. Det KAN være at metode1 og metode4 løses atskillig enklere hvis du allerede har implementert metode3.
-	Enkelte klasser kan være lagt opp til at du skal måtte skrive definisjonen av klassen, og metodene, selv. Les kravene, disse er gjerne definert i Javadocen som står over hver metode. Husk at når du definerer en klasse langt opp i en eksisterene fil med masse javadoc  (altså skriver ‘public KlasseNavn implements IInterface {‘ så er en typisk feil at en glemmer å legge inn den avsluttende } helt til slutt i filen. Sjekk dette!
- Hvis dere blir bedt om å lage en metode, og det er en del javadoc over (og du synes det er mye), så kan du gjøre følgende: definer metoden på rett plass: public void MetodeNavn() {}. Men med riktig navn da. Ikke tenk på riktig returverdi eller riktige parametre. Når metoden er definert, da kan du holde musa over metoden og se magien. Test det på [denne klassen}(https://gitlab.stud.idi.ntnu.no/tdt4100/eksamener/v2024-testeksamen/-/blob/main/src/main/java/com/diablo/manufacturing/examples/part2/PlayerCharacterRegister.java?ref_type=heads). Så leser du den fint formaterte dokumentasjonen for å se hva som skal være korrekt returtype og parametre. Og krav.
-	IKKE la det stå igjen røde streker i kode du ikke jobber med for tiden, altså kompileringsfeil. Slike feil vil gjøre det mye vanskeligere for verktøyet å hjelpe deg. Du vil også kunne få mindre poeng på grunn av det. Å la linjer med kompileringsfeil stå igjen i kode du leverer fra deg er såpass alvorlig at vi kan vurdere å stryke poeng på denne metoden helt.  Fiks derfor slike feil, eksempelvis gjennom å kommentere ut linjen med feil, eller å faktisk returnere en dummyverdi fra metoden.
-	Husk på å bruke muspekeren til din fordel. Hold den over en metode som kalles for å se hva javadocen om metoden sier, eller over metodenavnsdefinisjon for å se det samme. Virker også for klasser. 
- Husk at dere kan hoppe frem og tilbake mellom stedene dere har vært i koden. For Windows skal alt-venstre/høyrepil funke. Lærte noe nytt i dag også jeg gitt.
Og husk at Optional og Records er pensum i år - selv om de ikke har vært brukt masse på tidligere eksamener (optional var vel med i fjor...) Records er som det ble nevnt på forelesning en veldig enkel sak for å slippe å lage klasser som bare skal holde på data. Sjekk ut [koden som ble lagt](https://gitlab.stud.idi.ntnu.no/tdt4100/v2024/tdt4100-students-24/-/tree/main/foreksempel/src/main/java/uke16/records?ref_type=heads) ut for uke 16 hvis du ikke husker helt hvordan disse fungerer, da de ikke har dukket opp på noen tidligere eksamener. 

Lykke til!
