package com.diablo.manufacturing.examples.part2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.diablo.manufacturing.examples.part2.SolutionPlayerCharacterRegister;

public class SolutionPart2Test {
    
    @Test
    void addPlayerCharacterExistsNoCheckForReturnValue() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
    }

    @Test
    void getPlayerCharactersExists() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.getPlayerCharacters();
    }

    @Test
    void getPlayerCharactersReturnsCorrectSize() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        assertEquals(1, pcr.getPlayerCharacters().size());
    }


    @Test
    void getPlayerCharactersReturnsCorrectObject() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        assertTrue(pcr.getPlayerCharacters().get(0).getName().equals("Egrob"));
        assertTrue(pcr.getPlayerCharacters().get(0).getClassType().equals("Demon Hunter"));
    }

    @Test
    void addingPluralPlayerCharacters() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        assertEquals(3, pcr.getPlayerCharacters().size());
    }

    @Test
    void addingParagonPointsOnceToOnePCGivesIsStoredCorrectly() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addParagonLevel("Egrob", 10);
        assertEquals(10, pcr.getPlayerCharacters().get(0).getParagon_level());
    }

    @Test
    void sumOfAllNonExistantClassIsZero() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        assertEquals(0, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void sumOfOneParagonAddition() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addParagonLevel("Egrob", 10);
        assertEquals(10, pcr.totalParagons("Demon Hunter"));
    }


    @Test
    void sumOfTwoParagonAdditionsSameCharacterSumsUp() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addParagonLevel("Egrob", 10);
        pcr.addParagonLevel("Egrob", 10);
        assertEquals(20, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void sumOfTwoParagonAdditionsDifferentCharactersSumsUp() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        pcr.addParagonLevel("Egrob", 10);
        pcr.addParagonLevel("Gnyffe", 5);
        assertEquals(15, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void filteredPlayerCharactersReturnsCorrectSize() {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        assertEquals(2, pcr.filteredPlayerCharacters(pc -> pc.getClassType().equals("Demon Hunter")).size());
    }


     @Test
    public void writeResults_should_produce_correct_results() throws IOException {
        SolutionPlayerCharacterRegister pcr = new SolutionPlayerCharacterRegister();
        pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        pcr.addParagonLevel("Egrob", 10);
        pcr.addParagonLevel("Gnyffe", 5);
        pcr.addParagonLevel("Yffe", 15);

        

        FakeOutputStream fakeStream = new FakeOutputStream();
        pcr.writeStatus(fakeStream);
        List<String> stream = Arrays.stream(fakeStream.getWrittenString().split("\n")).toList();
        System.out.println(stream.toString());
        assertEquals(3, stream.size());
        assertEquals("Egrob, Demon Hunter, 10", stream.get(0));
        assertEquals("Gnyffe, Demon Hunter, 5", stream.get(1));
        assertEquals("Yffe, Witch Doctor, 15", stream.get(2));
    }

}
