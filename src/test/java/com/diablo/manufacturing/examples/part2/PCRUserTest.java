package com.diablo.manufacturing.examples.part2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.ntnu.tdt4100.examples.part2.PlayerCharacter;


/** I denne testklassen viser jeg et par av de tingene som dere kan komme borti 
 * på eksamen, rundt 'mocking' av klasser. Dette er ikke et krav for å kunne ha en
 * nytte av eksamen, dere kan uansett bare kjøre testene uten å tenke på mocking.
 * Metodenavnene på eksamen skal beskrive hva som skjer.
 * 
 * Merk at disse testene er kodet mot SolutionPlayerCharacterRegister, ikke 
 * PlayerCharacterRegister. Det er fordi denne ikke er definert ennå. Men det 
 * ikke noe i veien for å bruke PlayerCharacterRegister i stedet. Derimot er det
 * heller ikke noe grunn til det i dette eksempelet, da vi uansett bare mocker den!
 */

public class PCRUserTest {
    

    /**
     * Den normale måten å teste på, uten bruk av mock. For å vise 
     * hvordan det var i øvingene.
     */
    @Test
    void testGetNumberOfCharactersWithoutMock() {
        PCRUser pcrUser = new PCRUser(new SolutionPlayerCharacterRegister());
        assertEquals(0, pcrUser.getNumberOfCharacters());
        pcrUser.addCharacterRandomClass("Gandalf");
        assertEquals(1, pcrUser.getNumberOfCharacters());
        pcrUser.addCharacterRandomClass("Frodo");
        assertEquals(2, pcrUser.getNumberOfCharacters());
    }

        /** Her ser dere hvordan jeg kan bruke SolutionPlayerCharacterRegister som en mock
         * Jeg først sier ifra når noen kaller på getPlayerCharacters() så skal jeg returnere en tom liste
         * Deretter lager jeg en PCRUser med denne mocken, og kaller getNumberOfCharacters() på den.
         * Da skal jeg få 0 tilbake, ettersom listen er tom.
         * 
         * Deretter sier jeg ifra at når noen kaller på getPlayerCharacters() så skal jeg returnere en 
         * liste med én spiller. Dermed bør metoden getNumberOfCharacters() returnere 1. Tilsvarende med 2.
         */

    @Test 
    void testGetNumberOfCharactersWithMock() {
        SolutionPlayerCharacterRegister mock = mock(SolutionPlayerCharacterRegister.class);
        // Mock empty list when asked for player characters
        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>());

        PCRUser pcrUser = new PCRUser(mock);
        assertEquals(0, pcrUser.getNumberOfCharacters());

        // Mock 1 player character when asked for player characters
        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>(List.of(new PlayerCharacter("Gandalf", "Wizard"))));
        assertEquals(1, pcrUser.getNumberOfCharacters());

        // 2 players
        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>(List.of(new PlayerCharacter("Gandalf", "Wizard"), new PlayerCharacter("Frodo", "Monk"))));
        assertEquals(2, pcrUser.getNumberOfCharacters());
    }


    @Test
    void testGetPlayersOfClassWithoutMock() {
        PCRUser pcrUser = new PCRUser(new SolutionPlayerCharacterRegister());
        assertEquals("", pcrUser.getPlayersOfClass("Wizard"));

        pcrUser.addCharacterWithClass("Gandalf", "Wizard");
        assertEquals("Gandalf\n", pcrUser.getPlayersOfClass("Wizard"));

        pcrUser.addCharacterWithClass("Frodo", "Monk");
        assertEquals("Gandalf\n", pcrUser.getPlayersOfClass("Wizard"));

        pcrUser.addCharacterWithClass("hansomviikkeskalsinavnetpå", "Wizard");
        assertEquals("Gandalf\nhansomviikkeskalsinavnetpå\n", pcrUser.getPlayersOfClass("Wizard"));
    }

    @Test
    void testGetPlayersOfClassWithMock() {

        SolutionPlayerCharacterRegister mock = mock(SolutionPlayerCharacterRegister.class);

        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>());

        PCRUser pcrUser = new PCRUser(mock);
        assertEquals("", pcrUser.getPlayersOfClass("Wizard"));

        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>(List.of(new PlayerCharacter("Gandalf", "Wizard"))));
        assertEquals("Gandalf\n", pcrUser.getPlayersOfClass("Wizard"));

        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>(List.of(new PlayerCharacter("Gandalf",
                "Wizard"), new PlayerCharacter("Frodo", "Monk"))));
        assertEquals("Gandalf\n", pcrUser.getPlayersOfClass("Wizard"));

        when(mock.getPlayerCharacters()).thenReturn(new ArrayList<>(List.of(new PlayerCharacter("Gandalf",
                "Wizard"), new PlayerCharacter("Frodo", "Monk"), new PlayerCharacter("hansomviikkeskalsinavnetpå", "Wizard"))));
        assertEquals("Gandalf\nhansomviikkeskalsinavnetpå\n", pcrUser.getPlayersOfClass("Wizard"));
    }

    /** Her ser vi hvordan vi kan bruke monk for å registrere antallet kall en metode har fått. */
    @Test
    void showMockusingVerify () {
        SolutionPlayerCharacterRegister mock = mock(SolutionPlayerCharacterRegister.class);
        PCRUser pcrUser = new PCRUser(mock);
        pcrUser.addCharacterWithClass("Gandalf", "Wizard");
        pcrUser.addCharacterWithClass("Frodo", "Monk");
        pcrUser.addCharacterWithClass("hansomviikkeskalsinavnetpå", "Wizard");

        // Verifying that the method addPlayerCharacter was called 3 times
        verify(mock, times(3)).addPlayerCharacter(anyString(), anyString());
    }

}
