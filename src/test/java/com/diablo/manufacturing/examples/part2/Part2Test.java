package com.diablo.manufacturing.examples.part2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

// Uncomment the import statement below
//import com.diablo.manufacturing.examples.part2.PlayerCharacterRegister;

public class Part2Test {
    
    @Test
    void addPlayerCharacterExistsNoCheckForReturnValue() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
    }

    @Test
    void getPlayerCharactersExists() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);
        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.getPlayerCharacters();
    }

    @Test
    void getPlayerCharactersReturnsCorrectSize() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // assertEquals(1, pcr.getPlayerCharacters().size());
    }


    @Test
    void getPlayerCharactersReturnsCorrectObject() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // assertTrue(pcr.getPlayerCharacters().get(0).getName().equals("Egrob"));
        // assertTrue(pcr.getPlayerCharacters().get(0).getClassType().equals("Demon Hunter"));
    }

    @Test
    void addingPluralPlayerCharacters() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        // pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        // assertEquals(3, pcr.getPlayerCharacters().size());
    }

    @Test
    void addingParagonPointsOnceToOnePCGivesIsStoredCorrectly() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addParagonLevel("Egrob", 10);
        // assertEquals(10, pcr.getPlayerCharacters().get(0).getParagon_level());
    }

    @Test
    void sumOfAllNonExistantClassIsZero() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // assertEquals(0, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void sumOfOneParagonAddition() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addParagonLevel("Egrob", 10);
        // assertEquals(10, pcr.totalParagons("Demon Hunter"));
    }


    @Test
    void sumOfTwoParagonAdditionsSameCharacterSumsUp() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addParagonLevel("Egrob", 10);
        // pcr.addParagonLevel("Egrob", 10);
        // assertEquals(20, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void sumOfTwoParagonAdditionsDifferentCharactersSumsUp() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        // pcr.addParagonLevel("Egrob", 10);
        // pcr.addParagonLevel("Gnyffe", 5);
        // assertEquals(15, pcr.totalParagons("Demon Hunter"));
    }

    @Test
    void CorrectParametersToPlayerCharacterConstructorReturnsTrue() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // assertTrue(pcr.addPlayerCharacter("Egrob", "Demon Hunter"));
    }

    @Test
    void WrongParametersToPlayerCharacterConstructorReturnsFalse() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // assertFalse(pcr.addPlayerCharacter("Egrob", "Daemon Hunter"));
        // assertFalse(pcr.addPlayerCharacter("Python", "Demon Hunter"));
    }

        @Test
    void filteredPlayerCharactersReturnsCorrectSize() {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        // pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        // assertEquals(2, pcr.filteredPlayerCharacters(pc -> pc.getClassType().equals("Demon Hunter")).size());
    }

     @Test
    public void writeResults_should_produce_correct_results() throws IOException {
        // Uncomment test and remove the assertFalse to run the test
        assertFalse(true);

        // PlayerCharacterRegister pcr = new PlayerCharacterRegister();
        // pcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        // pcr.addPlayerCharacter("Yffe", "Witch Doctor");
        // pcr.addPlayerCharacter("Egrob", "Demon Hunter");
        // pcr.addParagonLevel("Egrob", 10);
        // pcr.addParagonLevel("Gnyffe", 5);
        // pcr.addParagonLevel("Yffe", 15);

        // FakeOutputStream fakeStream = new FakeOutputStream();
        // pcr.writeStatus(fakeStream);
        // List<String> stream = Arrays.stream(fakeStream.getWrittenString().split("\n")).toList();
        // assertEquals(3, stream.size());
        // assertEquals("Egrob, Demon Hunter, 10", stream.get(0));
        // assertEquals("Gnyffe, Demon Hunter, 5", stream.get(1));
        // assertEquals("Yffe, Witch Doctor, 15", stream.get(2));    
    }

}
