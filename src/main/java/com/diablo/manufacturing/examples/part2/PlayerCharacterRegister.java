package com.diablo.manufacturing.examples.part2;

// Imports will end up here.

/** public class PlayerCharacterRegister
 * Requirements:
 * <ul>
 * <li>The class must implement the Interface IPlayerCharacterRegister.</li>
 * <li>Other methods defined in the code must be implemented according to their respective JavaDoc descriptions.</li>
 * <li>Paragon is simply an integer that defines the strength of the player. A player starts out with paragon level 0.
 * </ul>
 * @author Børge
 */

 // TODO add class definition here
 // Remember that when you add a { at the end of the class definition here, you also need to add a } 
 // at the end of the file. You know, closing off the class.

    // TODO add class fields here


    /** public method addPlayerCharacter
     * Adds a player with a specific characterType to the register.
     * Behavioural requirements:
     * <ul>
     * <li>If PlayerCharacter (PC) has successfully been added, return true.
     * <li>All new players have paragon level 0. 
     * <li>If adding a PlayerCharacter for some reason throws an Exception, return false.</li>
     * <li>If PC with name already exists and it has a different characterType: 
     * delete the existing entry, add a new entry with name
     * and classType, paragon level 0, and return true.</li>
     * </ul>

     * 
     * @param name              The name of the PlayerCharacter - as a String
     * @param classType         The classType - as a String 
     * 
     * @return                  boolean: true if a PlayerCharacter is successfully added, otherwise false.
     * 
     * @see no.ntnu.tdt4100.examples.part2.PlayerCharacter
     * @see no.ntnu.tdt4100.examples.part2.ClassTypes
     */

    // TODO: add method addPlayerCharacter here


    /** public method getPlayerCharacters
     * Returns a copy of the List of existing PlayerCharacters.
     * 
     * @return Copy of the List of existing PlayerCharacters.
     */

    // TODO: add method getPlayerCharacters here


    /** public method addParagonLevel
     * Adds an integer of paragon levels to the PlayerCharacter with stated name.
     * <li>
     * Behavioural requirements:
     * <ul>
     * <li>Paragon levels are provided as int.
     * <li>Any possible exceptions that stem from adding levels to this PlayerCharacter 
     * are to be sent to the caller.</li>
     * <li>If there is no existing PlayerCharacter with this name, just ignore the request.</li>
     * </ul>

     * @param name - String
     * @param paragonLevel - int
     * @return void method
     */

    // TODO: add method addParagonLevel here

    /**
     * This method writes the current status of the register to the provided OutputStream.
     * @param outputStream - OutputStream that the status should be written to.
     * 
     * @throws IOException - If an I/O error occurs.
     * 
     * Behavioural requirements:
     * <ul>
     * <li>Write the status of the register to the provided OutputStream.</li>
     * <li>Players are to be sorted by ClassType, first letter first.</li>
     * <li>Players of the same ClassType should also be sorted by player names.</li>
     * <li>Each player should be written on a separate line.</li>
     * <li>Each line should contain the name of the player, the classType, and the paragon level.</li>
     * <li>Each field should be separated by a comma and a space.</li>
     * <li>For example: "Egrob, Demon Hunter, 10".</li>
     * 
     */
    // TODO: add method writeStatus here


   /** public method getPlayerCharacter
     * This method returns an Optional of a PlayerCharacter with the provided name.
     * @param name - String
     * @return The PlayerCharacter with the provided name, or an empty Optional if no such PlayerCharacter exists.
     */
    // TODO: add method getPlayerCharacter here


    // Might there be missing some methods resulting from implementing an Interface?
    // This text might possibly not be showing up on the actual exam, you need to
    // understand that you should implement it! Do as the description says.

    // Also remember to close off the entire class with }, right. it should fit with
    // the class definition.