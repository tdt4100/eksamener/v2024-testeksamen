package com.diablo.manufacturing.examples.part2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import no.ntnu.tdt4100.examples.part2.IPlayerCharacterRegister;
import no.ntnu.tdt4100.examples.part2.PlayerCharacter;

/** public class SolutionPlayerCharacterRegister
 * Requirements:
 * <ul>
 * <li>The class must implement the Interface IPlayerCharacterRegister.</li>
 * <li>Other methods defined in the code must be implemented according to their respective JavaDoc descriptions.</li>
 * </ul>
 * @author Børge
 */
public class SolutionPlayerCharacterRegister implements IPlayerCharacterRegister {

    // This could be solved with a List as well.
    List<PlayerCharacter> pcs = new ArrayList<>();


    /** public method addPlayerCharacter
     * Adds a player with a specific characterType to the register.
     * Behavioural requirements:
     * <ul>
     * <li>If PlayerCharacter (PC) has successfully been added, return true.
     * <li>If adding a PlayerCharacter for some reason throws an Exception, return false.</li>
     * <li>If PC with name already exists and it has a different characterType: 
     * delete the existing entry, add a new entry with name
     * and classType, with no paragon, and return true.</li>
     * </ul>
     * 
     * @param name              The name of the PlayerCharacter
     * @param classType         The classType
     * 
     * @return                  boolean: true if a PlayerCharacter is successfully added, otherwise false.
     * 
     * @see no.ntnu.tdt4100.examples.part2.PlayerCharacter
     */
    public boolean addPlayerCharacter(String name, String classType){
    try {
        PlayerCharacter existingPC = null;
        for (PlayerCharacter pc : pcs) {
            if (pc.getName().equals(name)) {
                existingPC = pc;
                break;
            }
        }
        if (existingPC != null) {
            if (!existingPC.getClassType().equals(classType)) {
                pcs.remove(existingPC);
                pcs.add(new PlayerCharacter(name, classType));
            }
        } else {
            pcs.add(new PlayerCharacter(name, classType));
        }
        return true;
    } catch (Exception e) { 
        // The following print is not described in method requirements
        // but is added for clarity.
        System.out.println("Something wrong with creating PlayerCharacter: "
            +e.getLocalizedMessage());
        return false;
    }

    // Alternative solution using Optional:
        // try {
        //     Optional<PlayerCharacter> existingPC = pcs.stream()
        //         .filter(pc -> pc.getName().equals(name))
        //         .findFirst();
        //     if (existingPC.isPresent()) {
        //         if (!existingPC.get().getClassType().equals(classType)) {
        //             pcs.remove(existingPC.get());
        //             pcs.add(new PlayerCharacter(name, classType));
        //         }
        //     } else {
        //         pcs.add(new PlayerCharacter(name, classType));
        //     }
        //     return true;
        // }
        // catch (Exception e) { 
        //     // The following print is not described in method requirements
        //     // but is added for clarity.
        //     System.out.println("Something wrong with creating PlayerCharacter: "
        //         +e.getLocalizedMessage());
        //     return false;
        // }

    }

    
    /** public method getPlayerCharacters
     * Returns a copy of the List of existing PlayerCharacters.
     * 
     * @return Copy of the List of existing PlayerCharacters.
     */
    public List<PlayerCharacter> getPlayerCharacters() {
        return new ArrayList<>(pcs);
    }

    /** public method addParagonLevel
     * Adds an integer of paragon levels to the PlayerCharacter with stated name.
     * <li>
     * Behavioural requirements:
     * <ul>
     * <li>Paragon levels are provided as int.
     * <li>Any possible exceptions that stem from adding levels to this PlayerCharacter 
     * are to be sent to the caller.</li>
     * <li>If there is no existing PlayerCharacter with this name, just ignore the request.</li>
     * </ul>

     * @param name - String 
     * @param paragonLevel - int
     * @return void method
     */

// TODO: add method addPlayerCharacter here

    public void addParagonLevel(String name, int paragonLevel) {
        boolean found = false;
        for (PlayerCharacter playerCharacter : pcs) {
            if (playerCharacter.getName().equals(name)) {
                found = true;
                playerCharacter.addParagon_level(paragonLevel);
            }
        }
        if (!found) {
            System.out.println("Debug: no existing PC called "+name+".");
        }
    }

    @Override
    public int totalParagons(String classType) {
        int sum = 0;
        for (PlayerCharacter playerCharacter : pcs) {
            if (playerCharacter.getClassType().equals(classType))
                sum += playerCharacter.getParagon_level();
        }
        return sum;

        // Alternative solution using streams:
        // return this.pcs.stream()
        //     .filter(p -> p.getClassType().equals(classType))
        //     .mapToInt(PlayerCharacter::getParagon_level)
        //     .sum();
    }

    @Override
    public List<PlayerCharacter> filteredPlayerCharacters(Predicate<PlayerCharacter> pred) {
        List<PlayerCharacter> result = new ArrayList<>();
        for (PlayerCharacter playerCharacter : pcs) {
            if (pred.test(playerCharacter))
                result.add(playerCharacter);
        }
        return result;

        // Quicker solution using streams:
        // return pcs.stream().filter(pred).toList();

    }



    /** public method getPlayerCharacter
     * This method returns an Optional of a PlayerCharacter with the provided name.
     * @param name - String
     * @return The PlayerCharacter with the provided name, or an empty Optional if no such PlayerCharacter exists.
     */
    public Optional<PlayerCharacter> getPlayerCharacter(String name) {
        for (PlayerCharacter playerCharacter : pcs) {
            if (playerCharacter.getName().equals(name))
                return Optional.of(playerCharacter);
        }
        return Optional.empty();

        // return pcs.stream()
        //     .filter(pc -> pc.getName().equals(name))
        //     .findFirst();

    }


    /**
     * This method writes the current status of the register to the provided OutputStream.
     * @param outputStream - OutputStream that the status should be written to.
     * @throws IOException - If an I/O error occurs.
     * 
     * Behavioural requirements:
     * <ul>
     * <li>Write the status of the register to the provided OutputStream.</li>
     * <li>Players are to be sorted by ClassType, first letter first.</li>
     * <li>Players of the same ClassType should also be sorted by player names.</li>
     * <li>Each player should be written on a separate line.</li>
     * <li>Each line should contain the name of the player, the classType, and the paragon level.</li>
     * <li>Each field should be separated by a comma and a space.</li>
     * <li>For example: "Egrob, Demon Hunter, 10".</li>
     * 
     */
    // TODO: add method writeStatus here

    public void writeStatus(OutputStream outputStream) {
        try {
            List<PlayerCharacter> sortedPCs = getPlayerCharacters();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            sortedPCs.sort((pc1, pc2) -> {
                if (pc1.getClassType().equals(pc2.getClassType())) {
                    return pc1.getName().compareTo(pc2.getName());
                }
                return pc1.getClassType().compareTo(pc2.getClassType());
            });
            for (PlayerCharacter pc : sortedPCs) {
                writer.write(pc.getName()+", "+pc.getClassType()+", "+pc.getParagon_level()+"\n");
            }
            writer.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /** Homemade main file, to check if actual behaviour equals intended behaviour also for
    things not covered by the provided tests. 
    In the actual exam, you will get a lot of help by using the provided tests, and you 
    should possibly use more tests rather than adding lots into mains. In the tests we often
    'fake' a lot of the surrounding code, so you test only the particular thing you focus on
    right now. */
    public static void main(String[] args) {
        SolutionPlayerCharacterRegister mpcr = new SolutionPlayerCharacterRegister();

        // Happy testing, making sure 
        System.out.println("Adding some characters:");
        mpcr.addPlayerCharacter("Egrob", "Demon Hunter");
        mpcr.addPlayerCharacter("Gnyffe", "Demon Hunter");
        mpcr.addPlayerCharacter("Yffe", "Witch Doctor");
        System.out.println(mpcr.pcs+"\nAdding some paragons:");
        mpcr.addParagonLevel("Egrob", 1);
        mpcr.addParagonLevel("Egrob", 1);
        mpcr.addParagonLevel("Gnyffe", 3);
        mpcr.addParagonLevel("Yffe", 4);
        System.out.println(mpcr.pcs+"\n");
        System.out.println("Total paragons for Witch Doctors: "+
            mpcr.totalParagons("Witch Doctor"));

        // Now to check if I cover the border cases - or more precisely stepping over them.
        /* have I implemented return type correctly? It might seem strange that
        addPlayerCharacter returns a true or false, as we never use it, but your
        code could be but a tiny cog in a large system. Read the requirements.*/
        System.out.println(mpcr.addPlayerCharacter("Devin", null));
        System.out.println(mpcr.addPlayerCharacter("Doctor Who", "Which Doctor"));
        System.out.println(mpcr.addPlayerCharacter("Onomatopoeia", "Witch Doctor"));
        System.out.println(mpcr.addPlayerCharacter("ShouldReturnTrue", "Monk"));

        // Adding paragon level to nonexisting PC should print my debug message.
        mpcr.addParagonLevel("Nothere", 100);

        // How do I handle adding same name?
        System.out.println("Egrob should only be a wizard now, with paragon 0:");
        mpcr.addPlayerCharacter("Egrob", "Wizard");
        mpcr.pcs.forEach(System.out::println);

        // Så bare vise hvordan Redord kan brukes
        // La oss lage en Record for å holde på dataene til hver spiller.
        // Merk at disse ikke kan endres på, det er kun til lagring.
        // La oss lage en Record med feltene name, classType og paragonLevel. Og passende typer.
        record PlayerCharacterRecord(String name, String classType, int paragonLevel) {};
        // Nå kan vi lage en liste med disse, og bruke den i stedet for PlayerCharacter.
        List<PlayerCharacterRecord> records = new ArrayList<>();
        for (PlayerCharacter pc : mpcr.pcs) {
            records.add(new PlayerCharacterRecord(pc.getName(), pc.getClassType(), pc.getParagon_level()));
        }

        // Sortere records etter navn
        records.sort((r1, r2) -> r1.name().compareTo(r2.name()));
        System.out.println("Sorted by name: "+records);

        // Sortere records etter paragonLevel (som jo bare er et tall)
        records.sort((r1, r2) -> r1.paragonLevel()-r2.paragonLevel());
        System.out.println("Sorted by name: "+records);

        // Som dere ser så er det enkelt å lage nye record-objekter, det er også enkelt å
        // bruke verdiene. Du må bare huske at de er immutable, så du kan ikke endre på dem.
        // De må også hentes ut med .feltnavn() og ikke .getFeltnavn().
    }
}
