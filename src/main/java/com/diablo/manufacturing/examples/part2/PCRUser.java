package com.diablo.manufacturing.examples.part2;

import java.util.Random;

import no.ntnu.tdt4100.examples.part2.ClassTypes;
import no.ntnu.tdt4100.examples.part2.PlayerCharacter;

public class PCRUser {
    SolutionPlayerCharacterRegister playerCharacterRegister;


    public PCRUser(SolutionPlayerCharacterRegister playerCharacterRegister) {
        this.playerCharacterRegister = playerCharacterRegister;
    }


    /**
     * 
     * @return The number of characters currently in the register - as an integer.
     */
    public int getNumberOfCharacters() {
        return playerCharacterRegister.getPlayerCharacters().size();
    }

    /**
     * Adds a character to the register with the given name and class type.
     * @param name - String
     * @param classType - String
     */
    public void addCharacterWithClass(String name, String classType) {
        playerCharacterRegister.addPlayerCharacter(name, classType);
    }


    /**
     * Adds a character to the register with the given name and a random class type.
     * @param name - String
     */
    public void addCharacterRandomClass(String name) {
        // Randomly select a class from ClassTypes
        Random random = new Random();
        int randomIndex = random.nextInt(ClassTypes.getValidBookingClasses().size()-1);
        playerCharacterRegister.addPlayerCharacter(name, ClassTypes.getValidBookingClasses().get(randomIndex));
    }

    /**
     * Returns a string of all players of a specific class type
     * I.e If the two players are of class type "Warrior", the string should be:
     * "Name1\nName2\n"
     * @param classType
     * @return String of all players of a specific class type on each line.
     */
    public String getPlayersOfClass(String classType) {
        return playerCharacterRegister.getPlayerCharacters().stream()
                .filter(pc -> pc.getClassType().equals(classType.toString()))
                .map(pc -> pc.getName())
                .reduce("", (a, b) -> a + b + "\n");
    }
}
