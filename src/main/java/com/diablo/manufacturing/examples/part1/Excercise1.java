package com.diablo.manufacturing.examples.part1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Excercise1 {
    
    /**
     * Study the class. Assume there are more methods,
     * but no method involves casting foo.
     * 
     * Comment out the line(s) that are true.
     */
    Collection<String> foo = new ArrayList<>();

    // Assume methods

    public void on_the_specter() {
        
        // TODO Uncomment the line(s) that are true
        // I_can_use_foo_append();
        // I_can_use_foo_get();
        // foo_cannot_be_iterated();
        // Changing_foo_to_class_LinkedList_gives_similar_behaviour();
    }

    /* 
    Poenget her er at du enten vet hva svaret er, eller du kan finne det ut.
    Oppgaven beskriver at vi har klassen Excercise1.
    Klassen inneholderen Collection<String> foo som er en ArrayList
    Klassen inneholder også metoden on_the_specter() som har 4 linjer som er kommentert ut.
    Du skal forvente at det ikke skjer noe casting av foo i noen av metodene.

    Gitt denne klassen, hvilke av de 4 linjene er sanne? 
    - Kan jeg bruke foo.append()? 
      Nei, ettersom foo følger Interface Collection, som ikke har append()
      Du kunne også teste dette ved å forsøke å skrive foo.append() i koden og se den røde streken.
    - Kan jeg bruke foo.get()? 
      Nei, ettersom foo følger Interface Collection, som ikke har get()
      Samme måte å sjekke på som den forrige.
    - Kan foo itereres? 
      Ja, ettersom foo er en ArrayList, som er en Collection, som kan itereres.
      Du kunne også sjekke dette ved å skrive en for-løkke som itererer over foo. Ingen røde streker.
    - Vil endring av implementasjonsklassen for foo fra ArrayList til LinkedList gi samme oppførsel? 
      Ja, ettersom LinkedList også er en Collection, og kan itereres.
      Du kunne også teste dette ved å endre foo til en LinkedList og se at koden fortsatt fungerer.
    */
     public static void main(String[] args) {
      
      Integer a = 2;
      Integer b = 2;
      Integer c = 3;
      Integer d = 4;
      List<Integer> list = new ArrayList<>(Arrays.asList(a, b, c, d));
      System.out.println(list);
      list.remove(2);
      System.out.println(list);
      list = new ArrayList<>(Arrays.asList(a, b, c, d));
      System.out.println(list);
      list.remove(b);
      System.out.println(list);
     }
}          
