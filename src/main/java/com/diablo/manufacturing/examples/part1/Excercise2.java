package com.diablo.manufacturing.examples.part1;

import java.util.ArrayList;
import java.util.List;

public class Excercise2 {
    
    /**
     * Study this class, and comment on its behaviour
     * 
     * Comment out the line(s) that are true.
     */

    protected int number;
    final List<Excercise2> spawn = new ArrayList<>();

    public void protect_your_assets() {

        for (int i = 0; i < spawn.size(); i++) {
            spawn.get(i).number = i;
        }

        spawn.sort((o1, o2) -> o2.number - o1.number);

        // TODO Uncomment the line(s) that are true

        // If_spawn_was_a_Collection_it_would_still_work();
        // Code_would_crash_on_runtime_because_of_visibility_issues();
        // Works_because_this_object_can_see_o1_and_o2_fields_that_use_protected();
        // Line_20_will_not_work_because_of_immutability(); // Var 21, det var en skrivefeil
        // protect_your_assets_crashes_on_spawn_size_0();

    }

    /*
     * This is a main method I have written to check which statements that
     * are correct. This means I can actually test things out.
     * Further, just changing List on line 16 to Collection lets me check
     * the first statement.
     * 
     * You can write main methods in your exam, but you could of course
     * write tests to help you check the behaviour instead. Mostly for part1 in the real exam.
     * There could also be tasks where creating new classes etc to check behaviour is also possible.
     */
    public static void main(String[] args) {
        Excercise2 e = new Excercise2();
        e.spawn.add(new Excercise2());
        e.spawn.add(new Excercise2());
        e.spawn.add(new Excercise2());
        e.protect_your_assets();
        for (Excercise2 excercise2 : e.spawn) {
            System.out.println(excercise2.number);
        }
    }
}
